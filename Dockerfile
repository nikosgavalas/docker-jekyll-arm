FROM arm64v8/ruby

RUN apt update
RUN gem install bundler jekyll github-pages jekyll-paginate

WORKDIR /srv/jekyll

CMD [ "/bin/sh" ]
